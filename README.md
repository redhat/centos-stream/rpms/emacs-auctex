# emacs-auctex

[AUCTeX](https://www.gnu.org/software/auctex/) is an extensible package that
supports writing and formatting TeX files for most variants of Emacs.

AUCTeX supports many different TeX macro packages, including AMS-TeX, LaTeX,
Texinfo and basic support for ConTeXt.  Documentation can be found under
/usr/share/doc, e.g. the reference card (tex-ref.pdf) and the FAQ.  The AUCTeX
manual is available in Emacs info (`C-h i d m AUCTeX RET`).  On the AUCTeX home
page, we provide manuals in various formats.

AUCTeX includes preview-latex support which makes LaTeX a tightly integrated
component of your editing workflow by visualizing selected source chunks (such
as single formulas or graphics) directly as images in the source buffer.
